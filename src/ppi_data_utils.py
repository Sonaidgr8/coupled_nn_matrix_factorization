import numpy as np
from collections import defaultdict
from collections import Counter
import operator
from sklearn import preprocessing
from sklearn import tree
from scipy.sparse import csgraph
import itertools
from sklearn.metrics.pairwise import cosine_similarity
from cvxopt import matrix, solvers
from commonFunctions import trace
from scipy import spatial
import networkx as nx
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsRestClassifier

def makeLabelMatrix():
    labelMatrix = np.loadtxt("ppi-dataset/truth.csv", delimiter=',', usecols=range(0,) , dtype=float , skiprows=0)
    return labelMatrix.T

def makeAdjMatrix():
    adjMatrix = np.loadtxt("ppi-dataset/adjmat.csv", delimiter=',', usecols=range(0,) , dtype=float , skiprows=0)
    return adjMatrix

def makeViewMatrix():
    viewMatrix = np.loadtxt("ppi-dataset/view.csv", delimiter=',', usecols=range(0,) , dtype=float , skiprows=0)
    return viewMatrix.T

def makeLabelVector(labelMatrix):
    labelVector = []
    
    for row in labelMatrix:
        k=1
        for field in row:
            if field==1:
                labelVector.append(str(k))
            k+=1
    all_label_dist = np.zeros(np.shape(labelMatrix)[1])
    for i in labelVector:
        all_label_dist[int(i)-1]+=1
    return np.array(labelVector), all_label_dist

def makeIndex(path, filename):
    filename = path + filename
    index = np.loadtxt(filename, delimiter=',', usecols=range(0,) , dtype=float , skiprows=0)
    
    train_sample_count=0
    for i in index:
        if i==1:
            train_sample_count+=1
    return index, train_sample_count

def generate_rel_info(adjMatrix, labelMatrix) :
    #print "adjMatrix", np.shape(adjMatrix), "x", "labelMatrix", np.shape(labelMatrix)
    relInfo = np.dot(np.array(adjMatrix), np.array(labelMatrix))
    #print "RelInfo: ", np.shape(rel_info)
    i=0
    for row in relInfo:
        total=0
        for each in row:
            total+=each
        
        if total!=0:
            j=0
            for each in row:
                relInfo[i][j]=float(each/total)
                j+=1
        i+=1
           
    return relInfo

def update_label_matrix(predicted, testList, labelMatrix, ppi_graph):
    for (each, pos) in zip(predicted, testList):
        labelMatrix[pos] = np.zeros(np.shape(labelMatrix)[1])
        labelMatrix[pos][int(each)-1] = 1
        
    labelVector, all_label_dist = makeLabelVector(labelMatrix)
    for pos in testList :
        ppi_graph.node_list[pos].label = labelVector[pos]
    #for i in range(np.shape(labelMatrix)[0]):
        #ppi_graph.node_list[i].label = labelVector[i]
    return labelMatrix, labelVector, ppi_graph
 
def load_ppi_data() :
    labelMatrix = makeLabelMatrix()
    adjMatrix = makeAdjMatrix()
    viewMatrix = makeViewMatrix()
    ground_truth_labelVector, all_label_dist = makeLabelVector(labelMatrix)
    no_of_nodes = np.shape(labelMatrix)[0]
    no_of_labels = np.shape(labelMatrix)[1]
    no_of_attrs = np.shape(viewMatrix)[1]
    reln_feature_vector = np.zeros(no_of_labels)
    
    return labelMatrix,adjMatrix,viewMatrix,ground_truth_labelVector,all_label_dist,no_of_nodes,no_of_labels,no_of_attrs
     
def ppi_test_train_split(path, filename, viewMatrix, labelVector, relInfo) :
    index, train_sample_count = makeIndex(path, filename)
    trainRelInfo, testRelInfo = [], []
    trainViewMatrix, testViewMatrix = [], []
    trainLabels, testLabels = [], []
    trainList, testList = [], []
    pos=0
    for i in index:
        if i==1:
            trainViewMatrix.append(viewMatrix[pos])
            trainLabels.append(labelVector[pos])
            trainList.append(pos)
            trainRelInfo.append(relInfo[pos])
        else:
            testViewMatrix.append(viewMatrix[pos])
            testLabels.append(labelVector[pos])
            testList.append(pos)
            testRelInfo.append(relInfo[pos])
        pos+=1   
    return index, train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testViewMatrix,testLabels,testList,testRelInfo

def is_Converged(preTransitionVector, transitionVector):
    res = np.array_equiv(preTransitionVector,transitionVector)
    return res

def compute_ERWR_Score(sample_count,adjMatrix):
    restartProbability = 0.2;
    
    temp_adjMatrix = np.array(adjMatrix)
    temp_adjMatrix = csgraph.laplacian(temp_adjMatrix, normed=True)
    #print temp_adjMatrix
    restartVector =  np.zeros(shape=(sample_count,1)) # e[i] (n x 1)
    transitionVector = np.zeros(shape=(sample_count,1)) # r[i] (n x 1)
    preTransitionVector = np.zeros(shape=(sample_count,1)) # previous value of r[i] (n x 1)
    rankingMatrix = np.zeros(shape=(sample_count,sample_count)) # R(n x n)
    
    no_of_iterations = 4
    for i in range(sample_count) :
        for j in range(sample_count):
            if i == j :
                restartVector[j] = 1.0
        transitionVector = np.array(restartVector)
        j = 0
        while True:
            preTransitionVector = np.array(transitionVector)
            transitionVector = np.dot(temp_adjMatrix,transitionVector)
            transitionVector = np.dot(temp_adjMatrix,transitionVector)
            transitionVector = preprocessing.normalize(transitionVector,norm='l1',axis=1)
            transitionVector = transitionVector * (1-restartProbability) + restartProbability * restartVector
            if j == no_of_iterations :
            #if isConverged(preTransitionVector, transitionVector):
                for k in range(sample_count) :
                    rankingMatrix[i][k] = transitionVector[k]
                #print transitionVector
                break
            j += 1
    #print rankingMatrix
    overall_similarity_matrix = preprocessing.normalize(rankingMatrix,norm='l1',axis=1)
    normalized_rankingMatrix = (overall_similarity_matrix-overall_similarity_matrix.min(0))/(overall_similarity_matrix.max(0)-overall_similarity_matrix.min(0))
    return np.array(normalized_rankingMatrix) 

def generate_Label_Similarity_Graph(train_sample_count,trainList,Y,index,adjMatrix):
    T3 = np.zeros((train_sample_count,train_sample_count))
    YY = np.zeros((Y.shape[0],Y.shape[0])) 
    adjMatrix = np.array(adjMatrix)
    G = nx.from_numpy_matrix(adjMatrix)
    for i in range(Y.shape[0]) :
        for j in range(Y.shape[0]) :
            if i != j and ( nx.has_path(G,i,j) or nx.has_path(G,j,i)) :
                cos_sim = 1 - spatial.distance.cosine(Y[i], Y[j])
                if nx.has_path(G,i,j) :
                    YY[i][j] = cos_sim
                    if index[i]==1 and index[j]==1 :
                        T3[trainList.index(i)][trainList.index(j)] = cos_sim
                if nx.has_path(G,j,i) :
                    YY[j][i] = cos_sim
                    if index[i]==1 and index[j]==1 :
                        T3[trainList.index(j)][trainList.index(i)] = cos_sim
            elif i==j :
                YY[i][i] = 1.0
    return T3, YY

def generate_Prediction_Similarity_Graph(train_sample_count,trainList,Y,index,viewMatrix):
    T4 = np.zeros((train_sample_count,train_sample_count))
    PP = np.zeros((Y.shape[0],Y.shape[0])) 
    pos=0
    x_train = []
    y_train = []
    for i in index:
        if i==1:
            x_train.append(viewMatrix[pos])
            y_train.append(Y[pos])
        pos+=1 
    clf = OneVsRestClassifier(LinearSVC(random_state=0)).fit(np.array(x_train), np.array(y_train))
    predicted = clf.decision_function(np.array(viewMatrix))
    for i in range(Y.shape[0]) :
        for j in range(Y.shape[0]) :
            pred_xi = predicted[i]
            pred_xj = predicted[j]
            cos_sim = 1 - spatial.distance.cosine(pred_xi, pred_xj)
            PP[i][j] = cos_sim
            PP[j][i] = cos_sim
            if index[i]==1 and index[j]==1 :
                T4[trainList.index(i)][trainList.index(j)] = cos_sim
                T4[trainList.index(j)][trainList.index(i)] = cos_sim
    return T4, PP

def latent_Graph_Generate_Integrate(index,train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testList,X,Y,q,D):
    T3, YY = generate_Label_Similarity_Graph(train_sample_count,trainList,Y,index,D[1]) # Takes label matrix and adjacencyy matrix D[1]
    T4, PP = generate_Prediction_Similarity_Graph(train_sample_count,trainList,Y,index,X) # Takes label matrix and adjacencyy matrix D[1]
    T5 = np.zeros((train_sample_count,train_sample_count))
    T6 = np.zeros((train_sample_count,train_sample_count))
    for i in trainList :
        for j in trainList :
            T5[trainList.index(i)][trainList.index(j)] = D[0][i][j]
            T6[trainList.index(i)][trainList.index(j)] = D[1][i][j]
    T7 = compute_ERWR_Score(train_sample_count,T6)
    #T7 = compute_ERWR_Score(train_sample_count,T5)
    T = [T3,T5,T6,T7]
    #T = [T3,T4,T5,T6,T7]
    #T = [T5,T7]
    m = len(T)
    # create label matrix of training data
    Y_t = np.zeros((train_sample_count,q)) 
    pos=0
    for i in index:
        if i==1:
            Y_t[trainList.index(pos)]=Y[pos]
        pos+=1   
    
    '''Solve the quadratic optimization problem '''
    delta = 0.01
    I = np.eye(m, M=m, k=0, dtype=float)
    G = np.eye(m, M=m, k=0, dtype=float)
    G = matrix(np.dot(-1,G))
    temp1 = np.dot(2*delta,I)
    temp3 = np.dot(Y_t,Y_t.T)
    Q = np.zeros((m,m),dtype=float)
    E = np.zeros((len(index),len(index)),dtype=float)
    K = np.zeros((m,m),dtype=float)
    u = np.zeros((m,1),dtype=float)
    W = np.zeros((m,1),dtype=float)
    P = np.zeros((m,1),dtype=float)
    h = matrix(np.zeros((m,1),dtype=float))
    A = matrix(np.ones((1,m),dtype=float))
    b = matrix(1.0)
    for i in range(m) :
        for j in range(m) :
            temp2 = np.dot(T[i].T,T[j])
            temp2 = np.dot(2,temp2)
            K[i][j] = trace(temp2)
        temp4 = np.dot(temp3,T[i])
        u[i] = trace(temp4)
    Q = matrix(np.add(K,temp1))
    temp4 = np.dot(-2,u.T)
    P = matrix(temp4.T)
    sol=solvers.qp(Q, P, G, h, A, b)
    W = sol['x']
    
    
    # Integrate latent graph
    D1 = []
    D1.append(YY)
    D1.append(PP)
    for elem in D :
        D1.append(elem)
    #print len(D1)
        
    m = len(D1)
    for a in range(len(index)) :
        for b in range(len(index)) :
            sum = 0.0
            for k in range(m) :
                sum = sum + np.dot(W[k],D1[k][a][b])
            E[a][b] = sum
    
    #print np.shape(E)
    #print E
    return E
