import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn import preprocessing
from sklearn import tree
from scipy.sparse import csgraph
from cvxopt import matrix, solvers
import cvxopt
import pandas as pd
from commonFunctions import trace
from collections import defaultdict
from collections import Counter
import operator
import itertools
from numpy import dtype


def make_Node_Dict():
    N = np.loadtxt("rugby-dataset/rugby.ids", delimiter=None, usecols=range(0,) , dtype=np.str , skiprows=0)
    n = np.shape(N)[0]
    N = N.tolist()
    return N,n

def make_Label_Matrix(n,N):
    C = np.loadtxt("rugby-dataset/community.ids", delimiter=None, usecols=range(0,) , dtype=np.str , skiprows=0)
    q = np.shape(C)[0]
    C = C.tolist()
    
    temp = np.genfromtxt("rugby-dataset/rugby.communities", delimiter=None , dtype=None ) 
    Y = np.zeros((q,n))
    i = 0
    for m in temp :
        a = m.strip().split(',')
        for elem in a :
            itemindex = N.index(elem)
            Y[i][itemindex]=1
        i = i + 1
    return C,q,Y.T
 
def make_View_Matrix(n,N): 
    df = pd.read_csv("rugby-dataset/rugby-tweets500.features", sep=" ",  skiprows=0, header=None, engine='python')
    F = df.values.tolist()
    m = len(F)
    temp = np.loadtxt("rugby-dataset/rugby-tweets500.mtx", delimiter=" " ,skiprows=1, dtype=None )
    temp.tolist()
    X = np.zeros((m,n))
    for mem in temp :
        X[int(mem[0])][N.index(str(int(mem[1])))]=1
    return F,m,X.T
 
def make_Adj_Matrices(n,N):
    members = np.loadtxt("rugby-dataset/rugby-follows.mtx", delimiter=" " ,skiprows=1, dtype=np.str )
    members.tolist()
    E1 = np.zeros((n,n))
    for mem in members :
        E1[N.index(str(int(mem[0])))][N.index(str(int(mem[1])))]=1
    
    members = np.loadtxt("rugby-dataset/rugby-mentions.mtx", delimiter=" " ,skiprows=1, dtype=np.str )
    members.tolist()
    E2 = np.zeros((n,n))
    for mem in members :
        E2[N.index(str(int(mem[0])))][N.index(str(int(mem[1])))]=1
    
    members = np.loadtxt("rugby-dataset/rugby-retweets.mtx", delimiter=" " ,skiprows=1, dtype=np.str )
    members.tolist()
    E3 = np.zeros((n,n))
    for mem in members :
        E3[N.index(str(int(mem[0])))][N.index(str(int(mem[1])))]=1
    return E1,E2,E3

def make_Label_Vector(labelMatrix):
    Z = defaultdict(set)
    i = 0
    for row in labelMatrix:
        k=1
        labelVector = []
        for field in row:
            if field==1:
                labelVector.append(str(k))
            k+=1
        Z[i] = labelVector
        i = i + 1
    all_label_dist = np.zeros(np.shape(labelMatrix)[1])
    for k , v in sorted(Z.items()) :
        for i in v:
            all_label_dist[int(i)-1]+=1
    return Z, all_label_dist

def generate_Rel_Info(E1,E2,E3,Y) :
    #print "adjMatrix", np.shape(adjMatrix), "x", "labelMatrix", np.shape(labelMatrix)
    relInfo1 = np.dot(np.array(E1), np.array(Y))
    relInfo2 = np.dot(np.array(E1.T), np.array(Y))
    relInfo3 = np.dot(np.array(E2), np.array(Y))
    relInfo4 = np.dot(np.array(E2.T), np.array(Y))
    relInfo5 = np.dot(np.array(E3), np.array(Y))
    relInfo6 = np.dot(np.array(E3.T), np.array(Y))
    relInfo =  np.concatenate((relInfo1,relInfo2,relInfo3,relInfo4,relInfo5,relInfo6),axis=1)
    i=0
    for row in relInfo:
        total=0
        for each in row:
            total+=each
        
        if total!=0:
            j=0
            for each in row:
                relInfo[i][j]=float(each/total)
                j+=1
        i+=1
    return relInfo

def make_Index(path, filename):
    filename = path + filename
    index = np.loadtxt(filename, delimiter=' ', usecols=range(0,) , dtype=float , skiprows=0)
    
    train_sample_count=0
    for i in index:
        if i==1:
            train_sample_count+=1
    return index, train_sample_count
   
def load_Rugby_Data():
    N,n = make_Node_Dict()
    C,q,Y = make_Label_Matrix(n,N)
    F,m,X = make_View_Matrix(n,N)
    E1,E2,E3 = make_Adj_Matrices(n,N)
    ground_truth_labelVector, all_label_dist = make_Label_Vector(Y)
    return N,n,C,q,Y,F,m,X,E1,E2,E3,ground_truth_labelVector, all_label_dist

def rugby_Test_Train_Split(path, filename, viewMatrix, labelVector, relInfo) :
    index, train_sample_count = make_Index(path, filename)
    trainRelInfo = defaultdict(set)
    testRelInfo = defaultdict(set)
    trainViewMatrix = defaultdict(set)
    testViewMatrix = defaultdict(set)
    trainLabels = defaultdict(set)
    testLabels = defaultdict(set)
    trainList, testList = [], []
    pos=0
    for i in index:
        if i==1:
            trainViewMatrix[pos]=viewMatrix[pos]
            trainLabels[pos]=labelVector.get(pos)
            trainList.append(pos)
            trainRelInfo[pos]=relInfo[pos]
        else:
            testViewMatrix[pos]=viewMatrix[pos]
            testLabels[pos]=labelVector.get(pos)
            testList.append(pos)
            testRelInfo[pos]=relInfo[pos]
        pos+=1   
    return index, train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testViewMatrix,testLabels,testList,testRelInfo

def generate_Label_Similarity_Graph(train_sample_count,Y,trainList):
    T2 = np.zeros((train_sample_count,train_sample_count))
    for n1, n2 in itertools.combinations(trainList, 2):
        cos_sim = cosine_similarity(Y[n1], Y[n2])
        T2[trainList.index(n1)][trainList.index(n2)] = cos_sim
        T2[trainList.index(n2)][trainList.index(n1)] = cos_sim
    return T2

def is_Converged(preTransitionVector, transitionVector):
    res = np.array_equiv(preTransitionVector,transitionVector)
    return res

def compute_ERWR_Score(train_sample_count,adjMatrix):
    restartProbability = 0.2;
    
    temp_adjMatrix = np.array(adjMatrix)
    temp_adjMatrix = csgraph.laplacian(temp_adjMatrix, normed=True)
    #print temp_adjMatrix
    restartVector =  np.zeros(shape=(train_sample_count,1)) # e[i] (n x 1)
    transitionVector = np.zeros(shape=(train_sample_count,1)) # r[i] (n x 1)
    preTransitionVector = np.zeros(shape=(train_sample_count,1)) # previous value of r[i] (n x 1)
    rankingMatrix = np.zeros(shape=(train_sample_count,train_sample_count)) # R(n x n)
    
    no_of_iterations = 4
    for i in range(train_sample_count) :
        for j in range(train_sample_count):
            if i == j :
                restartVector[j] = 1.0
        transitionVector = np.array(restartVector)
        j = 0
        while True:
            preTransitionVector = np.array(transitionVector)
            transitionVector = np.dot(temp_adjMatrix,transitionVector)
            transitionVector = np.dot(temp_adjMatrix,transitionVector) * (1-restartProbability) + restartProbability * restartVector
            if j == no_of_iterations :
            #if isConverged(preTransitionVector, transitionVector):
                for k in range(train_sample_count) :
                    rankingMatrix[i][k] = transitionVector[k]
                #print transitionVector
                break
            j += 1
    #print rankingMatrix
    overall_similarity_matrix = preprocessing.normalize(rankingMatrix,norm='l1',axis=1)
    normalized_rankingMatrix = (overall_similarity_matrix-overall_similarity_matrix.min(0))/(overall_similarity_matrix.max(0)-overall_similarity_matrix.min(0))
    return np.array(normalized_rankingMatrix) 

def latent_Graph_Generate_Integrate(index,train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testList,X,Y,q,D):
    #TT_1 = generate_Label_Similarity_Graph(train_sample_count,Y,trainList)
    #T2 = np.zeros((train_sample_count,train_sample_count))
    
    '''T3 = np.zeros((train_sample_count,train_sample_count))
    T4 = np.zeros((train_sample_count,train_sample_count))
    T5 = np.zeros((train_sample_count,train_sample_count))
    for i in trainList :
        for j in trainList :
            #T2[trainList.index(i)][trainList.index(j)] = D[0][i][j]
            #T3[trainList.index(i)][trainList.index(j)] = D[1][i][j]
            #T4[trainList.index(i)][trainList.index(j)] = D[2][i][j]
            #T5[trainList.index(i)][trainList.index(j)] = D[3][i][j]
            T3[trainList.index(i)][trainList.index(j)] = D[0][i][j]
            T4[trainList.index(i)][trainList.index(j)] = D[1][i][j]
            T5[trainList.index(i)][trainList.index(j)] = D[2][i][j]
    T6 = T3.T
    T7 = T4.T
    T8 = T5.T
    T9 = compute_ERWR_Score(train_sample_count,T3)
    T10 = compute_ERWR_Score(train_sample_count,T4)
    T11 = compute_ERWR_Score(train_sample_count,T5)
    T12 = compute_ERWR_Score(train_sample_count,T6)
    T13 = compute_ERWR_Score(train_sample_count,T7)
    T14 = compute_ERWR_Score(train_sample_count,T8)
    #T = [TT_1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14]
    T = [T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14]'''
    T3 = np.zeros((train_sample_count,train_sample_count))
    for i in trainList :
        for j in trainList :
            T3[trainList.index(i)][trainList.index(j)] = D[0][i][j]
    T9 = compute_ERWR_Score(train_sample_count,T3)
    T = [T3,T9]
    m = len(T)
    # create label matrix of training data
    Y_t = np.zeros((train_sample_count,q)) 
    pos=0
    for i in index:
        if i==1:
            Y_t[trainList.index(pos)]=Y[pos]
        pos+=1   
    
    '''Solve the quadratic optimization problem '''
    delta = 0.01
    I = np.eye(m, M=m, k=0, dtype=float)
    G = np.eye(m, M=m, k=0, dtype=float)
    G = matrix(np.dot(-1,G))
    temp1 = np.dot(2*delta,I)
    temp3 = np.dot(Y_t,Y_t.T)
    Q = np.zeros((m,m),dtype=float)
    E = np.zeros((len(index),len(index)),dtype=float)
    K = np.zeros((m,m),dtype=float)
    u = np.zeros((m,1),dtype=float)
    W = np.zeros((m,1),dtype=float)
    P = np.zeros((m,1),dtype=float)
    h = matrix(np.zeros((m,1),dtype=float))
    A = matrix(np.ones((1,m),dtype=float))
    b = matrix(1.0)
    for i in range(m) :
        for j in range(m) :
            temp2 = np.dot(T[i].T,T[j])
            temp2 = np.dot(2,temp2)
            K[i][j] = trace(temp2)
        temp4 = np.dot(temp3,T[i])
        u[i] = trace(temp4)
    Q = matrix(np.add(K,temp1))
    temp4 = np.dot(-2,u.T)
    P = matrix(temp4.T)
    sol=solvers.qp(Q, P, G, h, A, b)
    W = sol['x']
    
    
    '''Integrate Latent Graphs '''
    '''T1 = np.zeros((len(index),len(index)))
    for n1, n2 in itertools.combinations(trainList, 2):
        T1[trainList.index(n1)][trainList.index(n2)] = TT_1[trainList.index(n1)][trainList.index(n2)]
        T1[trainList.index(n2)][trainList.index(n1)] = TT_1[trainList.index(n2)][trainList.index(n1)]
    D1=[]
    D1.append(T1)'''
    D1 = []
    for elem in D :
        D1.append(elem)
    
    
    m = len(D1)
     
    for a in range(len(index)) :
        for b in range(len(index)) :
            sum = 0.0
            for k in range(m) :
                sum = sum + np.dot(W[k],D1[k][a][b])
            E[a][b] = sum
    
    #print np.shape(E)
    #print E
    return E
