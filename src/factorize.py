import numpy as np
import time
from numpy import dot, zeros, eye, empty, loadtxt, ones, dtype
from numpy.linalg import inv
import commonFunctions as cf
from scipy.sparse import csr_matrix, dok_matrix
from numpy import dot, kron, array, eye, savetxt
from numpy.linalg import qr, pinv, norm, inv 
from numpy.random import rand
from scipy.sparse.linalg import eigsh
from commonFunctions import squareFrobeniusNormOfSparse, matrixFitNormWithoutNormD, fitNormWWYAB
import json
from numpy import random
import numbers
import scipy.sparse as sp
from scipy import linalg


def check_random_state(seed):
    if seed is None or seed is np.random:
        return np.random.mtrand._rand
    if isinstance(seed, (numbers.Integral, np.integer)):
        return np.random.RandomState(seed)
    if isinstance(seed, np.random.RandomState):
        return seed
    raise ValueError('%r cannot be used to seed a numpy.random.RandomState'
' instance' % seed)
  
def safe_sparse_dot(a, b, dense_output=False):
    if sp.issparse(a) or sp.issparse(b):
        ret = a * b
        if dense_output and hasattr(ret, "toarray"):
            ret = ret.toarray()
        return ret
    else:
        return np.dot(a, b)

def randomized_range_finder(A, size, n_iter, random_state=None):
    
    random_state = check_random_state(random_state)

    # generating random gaussian vectors r with shape: (A.shape[1], size)
    R = random_state.normal(size=(A.shape[1], size))

    # sampling the range of A using by linear projection of r
    Y = safe_sparse_dot(A, R)
    del R

    # perform power iterations with Y to further 'imprint' the top
    # singular vectors of A in Y
    for i in xrange(n_iter):
        Y = safe_sparse_dot(A, safe_sparse_dot(A.T, Y))

    # extracting an orthonormal basis of the A range samples
    Q, R = linalg.qr(Y, mode='economic')
    return Q

def svd_flip(u, v, u_based_decision=True):
    if u_based_decision:
        # columns of u, rows of v
        max_abs_cols = np.argmax(np.abs(u), axis=0)
        signs = np.sign(u[max_abs_cols, xrange(u.shape[1])])
        u *= signs
        v *= signs[:, np.newaxis]
    else:
        # rows of v, columns of u
        max_abs_rows = np.argmax(np.abs(v), axis=1)
        signs = np.sign(v[xrange(v.shape[0]), max_abs_rows])
        u *= signs
        v *= signs[:, np.newaxis]
    return u, v

def randomized_svd(M, n_components, n_oversamples=10, n_iter=0,
                   transpose='auto', flip_sign=True, random_state=0):
    
    random_state = check_random_state(random_state)
    n_random = n_components + n_oversamples
    n_samples, n_features = M.shape

    if transpose == 'auto':
        transpose = n_samples < n_features
    if transpose:
        # this implementation is a bit faster with smaller shape[1]
        M = M.T

    Q = randomized_range_finder(M, n_random, n_iter, random_state)

    # project M to the (k + p) dimensional space using the basis vectors
    B = safe_sparse_dot(Q.T, M)

    # compute the SVD on the thin matrix: (k + p) wide
    Uhat, s, V = linalg.svd(B, full_matrices=False)
    del B
    U = np.dot(Q, Uhat)

    if flip_sign:
        if not transpose:
            U, V = svd_flip(U, V)
        else:
            # In case of transpose u_based_decision=false
            # to actually flip based on u and not v.
            U, V = svd_flip(U, V, u_based_decision=False)

    if transpose:
        # transpose back the results according to the input convention
        return V[:n_components, :].T, s[:n_components], U[:, :n_components].T
    else:
        return U[:, :n_components], s[:n_components], V[:n_components, :]
    
def _initialize_nmf(X, labelMatrix, index, n_components, proj, init=None, eps=1e-6, random_state=None):
    cf.check_non_negative(X, "NMF initialization")
    n_samples, n_features = X.shape
    
    # Default initialization
    if init is None:
        if n_components < n_features:
            init = 'nndsvd'
        else:
            init = 'random'

    # Random initialization
    elif init == 'random':
        avg = np.sqrt(X.mean() / n_components)
        rng = check_random_state(random_state)
        H = avg * rng.randn(n_components, n_features)
        W = avg * rng.randn(n_samples, n_components)
        np.abs(H, H)
        np.abs(W, W)
        return W, H
    
    # Eigen-vector based initialization
    elif init == 'nvecs':
        if n_samples == n_features : # When X is square matrix
            X[np.isnan(X)] = 0
            if np.allclose(X.transpose(), X) :
                avgX = X
            else :
                avgX = X + X.T
            eigvals, W = eigsh(sp.csc_matrix(X), n_components) 
            H = W.T
            W[W < eps] = 0
            H[H < eps] = 0
            avg = X.mean()
            W[W == 0] = avg
            H[H == 0] = avg
        else : 
            raise ValueError('Given matrix is not a square one so eigen-vector based decomposition is not possible \n')
        return W, H
    
    # Class-prior based initialization
    elif init == 'prior':
        n,q = labelMatrix.shape
        W = np.zeros((n,q))
        train_label_dist = np.zeros(np.shape(labelMatrix)[1])
        labelVector = []
        
        pos = 0
        for i in index:
            if i==1:
                k=1
                for field in labelMatrix[pos]:
                    if field==1:
                        labelVector.append(str(k))
                    k+=1
            pos += 1   
        
        for i in labelVector:
            train_label_dist[int(i)-1]+=1
            
        if q == n_components :
            pos = 0
            for i in index:
                if i==1:
                    W[pos] = labelMatrix[pos]
                else :
                    for j in range(q) :
                        W[pos][j] = train_label_dist[j]/sum(train_label_dist)
                pos += 1 
        # X = W.H :- Class-prior based initialization of W based on Label Matrix
        # and Random initialzation of H
        avg = np.sqrt(X.mean() / n_components)
        rng = check_random_state(random_state)
        H = avg * rng.randn(n_components, n_features)
        np.abs(H, H)
        
        return W, H          
                


    # NNDSVD initialization
    U, S, V = randomized_svd(X, n_components, random_state=random_state)
    W, H = np.zeros(U.shape), np.zeros(V.shape)
    # The leading singular triplet is non-negative
    # so it can be used as is for initialization.
    W[:, 0] = np.sqrt(S[0]) * np.abs(U[:, 0])
    H[0, :] = np.sqrt(S[0]) * np.abs(V[0, :])

    for j in range(1, n_components):
        x, y = U[:, j], V[j, :]

        # extract positive and negative parts of column vectors
        x_p, y_p = np.maximum(x, 0), np.maximum(y, 0)
        x_n, y_n = np.abs(np.minimum(x, 0)), np.abs(np.minimum(y, 0))

        # and their norms
        x_p_nrm, y_p_nrm = norm(x_p), norm(y_p)
        x_n_nrm, y_n_nrm = norm(x_n), norm(y_n)

        m_p, m_n = x_p_nrm * y_p_nrm, x_n_nrm * y_n_nrm

        # choose update
        if m_p > m_n:
            u = x_p / x_p_nrm
            v = y_p / y_p_nrm
            sigma = m_p
        else:
            u = x_n / x_n_nrm
            v = y_n / y_n_nrm
            sigma = m_n

        lbd = np.sqrt(S[j] * sigma)
        W[:, j] = lbd * u
        H[j, :] = lbd * v

    W[W < eps] = 0
    H[H < eps] = 0

    if init == "nndsvd":
        pass
    elif init == "nndsvda":
        avg = X.mean()
        W[W == 0] = avg
        H[H == 0] = avg
    elif init == "nndsvdar":
        rng = check_random_state(random_state)
        avg = X.mean()
        W[W == 0] = abs(avg * rng.randn(len(W[W == 0])) / 100)
        H[H == 0] = abs(avg * rng.randn(len(H[H == 0])) / 100)
    else:
        raise ValueError(
            'Invalid init parameter: got %r instead of one of %r' %
            (init, (None, 'random', 'nndsvd', 'nndsvda', 'nndsvdar', 'nvecs', 'prior')))

    return W, H

class NMF_Base(object):
    def __init__(self, max_iter=200, n_components=None, lmbda=1., alpha=1., beta=1., gamma=1., theta=1., l1_ratio=0., kl_divergence=False, init=None, proj=False,
                 solver='NMF_MU', preheatnum = 1, conv=1e-6,  random_state=None, verbose=0, shuffle=False,
                 flags=np.array([1,0,0,0,0,0]),nls_max_iter=2000, sparseness=None, zeta=1, eta=0.1):
        self.max_iter = max_iter
        self.n_components = n_components
        self.init = init
        self.proj = proj
        self.solver = solver
        self.conv = conv
        self.preheatnum = preheatnum
        self.random_state = random_state
        self.lmbda = lmbda
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.theta = theta
        self.l1_ratio = l1_ratio
        self.kl_divergence = kl_divergence
        self.verbose = verbose
        self.shuffle = shuffle
        self.flags = flags
        if sparseness is not None:
            print("Controlling regularization through the sparseness,"
                          " beta and eta arguments is only available"
                          " for 'pg' solver, which will be removed"
                          " in release 0.19. Use another solver with L1 or L2"
                          " regularization instead.")
        self.nls_max_iter = nls_max_iter
        self.sparseness = sparseness
        self.zeta = zeta
        self.eta = eta
        #raise NotImplementedError('NMF_Base is a base class that cannot be instantiated')

    def run(self, fi, selection, X, R, L, Y, WW, index, path):
        r = self.n_components
        max_iter = self.max_iter
        conv = self.conv
        preheatnum = self.preheatnum
        lmbda = self.lmbda
        alpha = self.alpha
        beta = self.beta
        gamma = self.gamma
        theta = self.theta
        l1_ratio = self.l1_ratio
        init = self.init
        proj = self.proj
        solver = self.solver
        shuffle = self.shuffle
        verbose = self.verbose
        flags = self.flags
        random_state=None
               
        info = {'r': r,
                'alg': str(self.__class__),
                'X_dim_1': X.shape[0],
                'X_dim_2': X.shape[1],
                'X_type': str(X.__class__),
                'max_iter': max_iter,
                'verbose': verbose }
        if verbose >= 0:
            print '[NMF] Running: '
            print json.dumps(info, indent=4, sort_keys=True)
            
        fi.write('[Config] rank: %d | maxIter: %d | conv: %7.1e | lmbda: %7.1e' % (r,max_iter, conv, lmbda))
        m,n = X.shape
        dtype = X.dtype 
        cf.check_non_negative(X, "NMF (input X)")
        cf.check_non_negative(R, "NMF (input R)")
        cf.check_non_negative(Y, "NMF (input Y)")
        L = np.maximum(L, 1e-5)
        cf.check_non_negative(L, "NMF (input L)")
        # precompute norms of X 
        normX = cf.squareFrobeniusNormOfSparse(X)
        normR = cf.squareFrobeniusNormOfSparse(R)
        normL = cf.squareFrobeniusNormOfSparse(L)
        normWWY = cf.squareFrobeniusNormOfSparse(WW * Y)
        fi.write('[Algorithm] The extended view matrix norm: %.5f' % normX)
        fi.write('[Algorithm] The extended adjacency matrix norm: %.5f' % normR)
        fi.write('[Algorithm] The extended improved adjacency matrix norm: %.5f' % normL)
        fi.write('[Algorithm] The extended WW * Y matrix norm: %.5f' % normWWY)
        # Initialization of factor matrices
        A = np.zeros((n,r))
        V = np.zeros((m,r))
        W = np.zeros((n,r))
        B = np.zeros((r,r))
        # Random initialization of B
        avg = np.sqrt(X.mean() / r)
        rng = check_random_state(random_state)
        B = avg * rng.randn(r, r)
        np.abs(B, B)
        
        if ((flags[1]==1 and flags[2]==1 and flags[3]==1 and flags[4]==1) and ( selection in [1,9,10])) :
            M = X 
            normM = normX
            A, VV = _initialize_nmf(M.T,Y.T,index,r,proj,init,eps=1e-6,random_state=None)
            AA, Vt = _initialize_nmf(M.T,Y.T,index,r,proj,init='nndsvda',eps=1e-6,random_state=None)
            V = Vt.T
            #print 'A shape' ,A.shape
            #print 'V shape' ,V.shape
            if selection == 1 :
                print ' Atribute latent-space classification + Factorize label matrix '
            elif selection == 9 :
                print ' Atribute latent-space classification + Network regularization  + Factorize label matrix '
            elif selection == 10 :
                print ' Atribute latent-space classification + Network regularization  + Latent Graph Integration + Factorize label matrix '
        elif ((flags[1]==1 and flags[2]==1 and flags[3]==1 and flags[4]==1) and ( selection in [2,3,11,12,13])) :
            if selection in [3,13] :
                N = L
                normN = normL
                if selection == 3 :
                    print ' Improved Network latent-space classification + Factorize label matrix '
                elif selection == 13 :
                    print ' Improved Network latent-space classification  + Network regularization  + Latent Graph Integration + Factorize label matrix '
            if selection in [2,11,12]:
                N = R
                normN = normR
                if selection == 2 : 
                    print ' Network latent-space classification + Factorize label matrix '
                elif selection == 11 : 
                    print ' Network latent-space classification  + Network regularization  + Factorize label matrix '
                elif selection == 12 : 
                    print ' Network latent-space classification  + Network regularization  + Latent Graph Integration  + Factorize label matrix '
            
            A, WW = _initialize_nmf(N.T,Y.T,index,r,proj,init,eps=1e-6,random_state=None)
            AA, Wt = _initialize_nmf(N.T,Y.T,index,r,proj,init='nndsvda',eps=1e-6,random_state=None)
            W = Wt.T
            #print 'A shape' ,A.shape
            #print 'W shape' ,W.shape
        elif ((flags[1]==1 and flags[2]==1 and flags[3]==1 and flags[4]==1) and (selection in [4,5,6,7,8] )) :
            M = X
            normM = normX
            N = R
            normN = normR
            if selection in [5,8] :
                N = L
                normN = normL
                if selection == 5 : 
                    print ' Atribute + Improved Network shared representation learning + Factorize label matrix '
                elif selection == 8 : 
                    print ' Atribute + Improved Network shared representation learning + Network regularization + Latent Graph Integration + Factorize label matrix '
            elif selection == 4 :
                print ' Atribute + Network shared representation learning + Factorize label matrix '
            elif selection == 6 :
                print ' Atribute + Network shared representation learning + Network regularization + Factorize label matrix ' 
            elif selection == 7 :
                print ' Atribute + Network shared representation learning + Network regularization + Latent Graph Integration + Factorize label matrix '
            A, VV = _initialize_nmf(M.T,Y.T,index,r,proj,init,eps=1e-6,random_state=None)
            AA, Vt = _initialize_nmf(M.T,Y.T,index,r,proj,init='nndsvda',eps=1e-6,random_state=None)
            V = Vt.T
            AA, Wt = _initialize_nmf(N.T,Y.T,index,r,proj,init='nndsvda',eps=1e-6,random_state=None)
            W = Wt.T
            #print 'A shape' ,A.shape
            #print 'V shape' ,V.shape 
            #print 'W shape' ,W.shape 
        else :
            print ' Something went wrong !! ' 
            
        fi.write('[Algorithm] Finished initialization.')
        
        # compute factorization
        fit = fitMAV = fitNAW = fitchange = fitold = 0
        exectimes = []
        total_time = 0
        if verbose >= 1:
            his = {'iter': [], 'elapsed': [], 'rel_error': []}
        start = time.time()
        for iterNum in xrange(max_iter):
            start_iter = time.time()
            # algorithm-specific iteration solver
            (A, V, W, B) = self.iter_solver(selection, flags, M, N, L, A, V, W, Y, WW, B, lmbda, alpha, beta, gamma, theta, l1_ratio)
            # reset entries of A for  the training data
            pos = 0
            for i in index:
                if i==1:
                    A[pos] = Y.T[pos]
                pos += 1 
                
            elapsed = time.time() - start_iter
            fit = 0
            regularizedFit = 0
            extRegularizedFit = 0
            fitMAV = 0
            fitNAW = 0
            fitWWYBAt = 0
            if iterNum >= preheatnum:
                fitWWYBAt = fitNormWWYAB(WW,Y,A,B)
                if lmbda != 0 :
                    regularizedFit = lmbda*(norm(A)**2)
                if lmbda != 0 and ( selection in [1,2,3,9,10,11,12,13]) : 
                    if selection in [1,9,10] :
                        extRegularizedFit = lmbda*(norm(V)**2) 
                        fitMAV = normM + matrixFitNormWithoutNormD(M, A, V)
                        fit += (0.5*fitMAV + 0.5*fitWWYBAt + regularizedFit + extRegularizedFit)/(normM + normWWY)
                    elif selection in [2,3,11,12,13] :
                        extRegularizedFit = lmbda*(norm(W)**2) 
                        fitNAW = normN + matrixFitNormWithoutNormD(N, A, W) 
                        fit += (0.5*fitNAW + 0.5*fitWWYBAt + regularizedFit + extRegularizedFit)/(normN + normWWY)
                if lmbda != 0 and ( selection in [4,5,6,7,8] ):
                    extRegularizedFit += lmbda*(norm(V)**2) 
                    extRegularizedFit += lmbda*(norm(W)**2) 
                    fitNAW = normN + matrixFitNormWithoutNormD(N, A, W)
                    fitMAV = normM + matrixFitNormWithoutNormD(M, A, V)
                    fit += (0.5*fitMAV + 0.5*fitNAW + 0.5*fitWWYBAt + regularizedFit + extRegularizedFit)/(normM + normN + normWWY)
            else :
                    fi.write('[Algorithm] Preheating is going on.\n')
            if verbose >= 1:
                his['iter'].append(iterNum+1)
                his['elapsed'].append(elapsed)
                his['rel_error'].append(fit)
                
                if verbose >= 2:
                    print 'iter:' + str(iterNum+1) + ', elapsed:' + str(elapsed) + ', rel_error:' + str(fit)
            
            exectimes.append(elapsed)
            fitchange = abs(fitold - fit)
            fi.write('[%3d] total fit: %.10f |  matrix fit: %.10f | delta: %.10f | secs: %.5f' % (iterNum, 
            fit, fitMAV, fitchange, exectimes[-1]))
                
            fitold = fit
            if iterNum > preheatnum and fitchange < conv:
                break
            
        #W, H, weights = mu.normalize_column_pair(W, H)
        # print the matrices of latent embeddings
        a_log = path + "A.log"
        v_log = path + "V.log"
        w_log = path + "W.log"
        b_log = path + "B.log"
        savetxt(a_log, A)
        savetxt(v_log, V)
        savetxt(w_log, W)
        savetxt(b_log, B)
        
        final = {}
        final['norm_X'] = normX
        final['rel_error'] = fit
        final['iterations'] = iterNum
        final['elapsed'] = time.time() - start

        rec = {'info': info, 'final': final}
        if verbose >= 1:
            rec['his'] = his

        if verbose >= 0:
            print '[NMF] Completed: '
            print json.dumps(final, indent=4, sort_keys=True)
            
        return (A, V, rec)
        

    def iter_solver(self, selection, flags, M, N, L, A, V, W, Y, WW, B, lmbda, alpha, beta, gamma, theta, l1_ratio):
        raise NotImplementedError

class NMF_MU(NMF_Base):
    def __init__(self, **kwargs):
        super(NMF_MU, self).__init__(**kwargs)
    
    def iter_solver(self, selection, flags, X, R, L, A, V, W, Y, WW, B, lmbda, alpha, beta, gamma, theta, l1_ratio):
        n, rank = A.shape 
        m = np.shape(X)[0]  
        At = A.T
        AtA = dot(At,A)
        D = np.zeros((n, n), dtype=np.float64)   
        
        if selection in [1,9,10] :
            # update V
            tmp1 = alpha *  dot(X,A)
            #print ('tmp1 ',tmp1)
            tmp2 = dot(V,(alpha * AtA)) + (V * lmbda)
            #print ('tmp2 ',tmp2)
            V = V * tmp1
            V = V / tmp2
            V = np.maximum(V, 1e-5)
            beta = 0.
            if selection == 1 :
                gamma = 0.
            if selection == 9 :
                L = R
            if selection in [9,10] :
                for i in range(n):
                    s = 0.0
                    for j in range(n) :
                        s = s + L[i][j]
                    D[i][i] = s
                
        elif selection in [2,3,11,12,13]  :
            if selection in [2,3] :
                gamma = 0.
                if selection == 3 :
                    R = L
            if selection in [11,12,13] :
                if selection == 11 :
                    L = R
                if selection == 13 :
                    R = L
                for i in range(n):
                    s = 0.0
                    for j in range(n) :
                        s = s + L[i][j]
                    D[i][i] = s
            # update W
            tmp1 = beta *  dot(R,A)
            #print ('tmp1 ',tmp1)
            tmp2 = dot(W,(beta * AtA)) + (W * lmbda)
            #print ('tmp2 ',tmp2)
            W = W * tmp1
            W = W / tmp2
            W = np.maximum(W, 1e-5)
            alpha = 0.
            
        elif selection in [4,5,6,7,8]:
            if selection in [4,5] :
                gamma = 0.
                if selection == 5 :
                    R = L
            if selection in [6,7,8]:
                if selection == 6 :
                    L = R
                if selection == 8 :
                    R = L
                for i in range(n):
                    s = 0.0
                    for j in range(n) :
                        s = s + L[i][j]
                    D[i][i] = s
            # update V
            tmp1 = alpha *  dot(X,A)
            #print ('tmp1 ',tmp1)
            tmp2 = dot(V,(alpha * AtA)) + (V * lmbda)
            #print ('tmp2 ',tmp2)
            V = V * tmp1
            V = V / tmp2
            V = np.maximum(V, 1e-5)
            # update W
            tmp1 = beta *  dot(R,A)
            #print ('tmp1 ',tmp1)
            tmp2 = dot(W,(beta * AtA)) + (W * lmbda)
            #print ('tmp2 ',tmp2)
            W = W * tmp1
            W = W / tmp2
            W = np.maximum(W, 1e-5)
        # update B
        tmp1 = theta *  dot((WW * Y), A)
        #print ('tmp1 ',tmp1)
        tmp2 = theta * dot( (WW * dot(B , At)) , A ) + (B * lmbda)
        #print ('tmp2 ',tmp2)
        B = B * tmp1
        B = B / tmp2
        B = np.maximum(B, 1e-5)  
        # update A
        Vt = V.T
        Rt = R.T
        Xt = X.T
        Yt = Y.T
        VtV = dot(Vt,V)
        Wt = W.T
        WtW = dot(Wt,W)
        tmp1 = alpha *  dot(Xt,V) + beta *  dot(Rt,W) + gamma * dot(L,A) + theta * dot( (WW.T * Yt) , B)
        #print ('tmp1 ',tmp1)
        tmp2 = dot(A,(alpha * VtV)) + dot(A,(beta * WtW)) + (A * lmbda) + gamma * dot(D,A) + theta * dot((WW.T * dot(A,B.T)),B)
        #print ('tmp2 ',tmp2)
        A = A * tmp1
        A = A / tmp2
        A = np.maximum(A, 1e-5)

        return (A, V, W, B)

