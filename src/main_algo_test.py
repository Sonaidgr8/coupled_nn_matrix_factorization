import rugby_data_utils
import ppi_data_utils
import factorize
from sklearn import metrics
from sklearn import tree
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsRestClassifier
import logging, time, argparse
from numpy import dot, kron, array, eye, savetxt
from numpy.linalg import qr, pinv, norm, inv 
from numpy.random import rand
from scipy.sparse.linalg import eigsh
import scipy.sparse
from commonFunctions import squareFrobeniusNormOfSparse, matrixFitNormWithoutNormD
import numpy as np
import sys
import time
from collections import defaultdict, Counter
import operator
import xlsxwriter
import ConfigParser
from sklearn.decomposition import NMF
from factorize import NMF_MU, NMF_Base
from sklearn.metrics import coverage_error, accuracy_score, hamming_loss
from scipy import spatial

ppi_data_dict = {2 : [['perc2_1.csv', 'perc2_2.csv', 'perc2_3.csv', 'perc2_4.csv', 'perc2_5.csv'],"ppi/perc2/"],
                 4 : [['perc4_1.csv', 'perc4_2.csv', 'perc4_3.csv', 'perc4_4.csv', 'perc4_5.csv'], "ppi/perc4/"],
                 6 : [['perc6_1.csv', 'perc6_2.csv', 'perc6_3.csv', 'perc6_4.csv', 'perc6_5.csv'], "ppi/perc6/"],
                 8 : [['perc8_1.csv', 'perc8_2.csv', 'perc8_3.csv', 'perc8_4.csv', 'perc8_5.csv'], "ppi/perc8/"],
                 10 : [['perc10_1.csv', 'perc10_2.csv', 'perc10_3.csv', 'perc10_4.csv', 'perc10_5.csv'], "ppi/perc10/"]}

#ppi_data_dict = {2 : [['perc2_1.csv', 'perc2_2.csv', 'perc2_3.csv', 'perc2_4.csv', 'perc2_5.csv'],"ppi/perc2/"]}

'''rugby_data_dict = {2 : [['perc2_1.csv', 'perc2_2.csv', 'perc2_3.csv', 'perc2_4.csv', 'perc2_5.csv'],"rugby/perc2/"],
                 4 : [['perc4_1.csv', 'perc4_2.csv', 'perc4_3.csv', 'perc4_4.csv', 'perc4_5.csv'], "rugby/perc4/"],
                 6 : [['perc6_1.csv', 'perc6_2.csv', 'perc6_3.csv', 'perc6_4.csv', 'perc6_5.csv'], "rugby/perc6/"],
                 8 : [['perc8_1.csv', 'perc8_2.csv', 'perc8_3.csv', 'perc8_4.csv', 'perc8_5.csv'], "rugby/perc8/"],
                 10 : [['perc10_1.csv', 'perc10_2.csv', 'perc10_3.csv', 'perc10_4.csv', 'perc10_5.csv'], "rugby/perc10/"],
                 20 : [['perc20_1.csv', 'perc20_2.csv', 'perc20_3.csv', 'perc20_4.csv', 'perc20_5.csv'], "rugby/perc20/"],
                 30 : [['perc30_1.csv', 'perc30_2.csv', 'perc30_3.csv', 'perc30_4.csv', 'perc30_5.csv'], "rugby/perc30/"],
                 40 : [['perc40_1.csv', 'perc40_2.csv', 'perc40_3.csv', 'perc40_4.csv', 'perc40_5.csv'], "rugby/perc40/"],
                 50 : [['perc50_1.csv', 'perc50_2.csv', 'perc50_3.csv', 'perc50_4.csv', 'perc50_5.csv'], "rugby/perc50/"]}'''
rugby_data_dict = {40 : [['perc40_1.csv', 'perc40_2.csv', 'perc40_3.csv', 'perc40_4.csv', 'perc40_5.csv'], "rugby/perc40/"]}
        
def config_section_map(config, section):
    param_dict = {}
    options = config.options(section)
    for option in options:
        try:
            param_dict[option] = config.get(section, option)
            if param_dict[option] == -1:
                print ("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            param_dict[option] = None
    return param_dict   

def naive_accuracy(true, pred):
    number_correct = 0
    i = 0
    for i, y in enumerate(true):
        if pred[i] == y:
            number_correct += 1.0
    return number_correct / len(true)

def hamming_score(y_true, y_pred, normalize=True, sample_weight=None):
    acc_list = []
    for i in range(y_true.shape[0]):
        set_true = set( np.where(y_true[i])[0] )
        set_pred = set( np.where(y_pred[i])[0] )
        #print('\nset_true: {0}'.format(set_true))
        #print('set_pred: {0}'.format(set_pred))
        tmp_a = None
        if len(set_true) == 0 and len(set_pred) == 0:
            tmp_a = 1
        else:
            tmp_a = len(set_true.intersection(set_pred))/\
                    float( len(set_true.union(set_pred)) )
        #print('tmp_a: {0}'.format(tmp_a))
        acc_list.append(tmp_a)
    return np.mean(acc_list)

def main_algo_test():    
    start_time = time.time()
    fi = open('out.txt', 'w')
    #fo = open('results.txt','w')
    #Fetching parameters from configuration file
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    param_dict = config_section_map(config,config.sections()[0])
    dataset = str(param_dict['dataset'])
    max_iter = int(param_dict['max_iter']) 
    no_of_latent_components = int(param_dict['no_of_latent_components'])
    lmbda = float(param_dict['lmbda'])
    alpha = float(param_dict['alpha'])
    beta = float(param_dict['beta'])
    gamma = float(param_dict['gamma'])
    theta = float(param_dict['theta'])
    l1_ratio = float(param_dict['l1_ratio'])
    kl_divergence = bool(param_dict['kl_divergence'])
    init = str(param_dict['init'])
    proj = bool(param_dict['proj']) 
    preheatnum = int(param_dict['preheatnum'])
    conv = float(param_dict['conv'])
    solver = str(param_dict['solver'])
    classification_only = int(param_dict['classification_only'])
    latent_space = int(param_dict['latent_space']) 
    integrate_LG = int(param_dict['integrate_lg']) 
    enforce_Y = int(param_dict['enforce_y']) 
    factorize_Y = int(param_dict['factorize_y']) 
    birelational = int(param_dict['birelational'])
    flags = np.array([classification_only,latent_space,integrate_LG,enforce_Y,factorize_Y,birelational])
    kargs = {'max_iter':max_iter,'n_components':no_of_latent_components,'lmbda':lmbda,'alpha':alpha,'beta':beta,'gamma':gamma,'theta':theta,'l1_ratio':l1_ratio,'kl_divergence':kl_divergence,'init':init,'proj':proj,'solver':solver,'preheatnum':preheatnum,'conv':conv,'random_state':None,'verbose':0,'shuffle':False,
             'flags':flags,'nls_max_iter':2000,'sparseness':None,'zeta':1,'eta':0.1}    
                 
    # Dataset handling code
    data_dict = defaultdict(set)
    if dataset == 'ppi' :
        data_dict = ppi_data_dict
        labelMatrix,adjMatrix,viewMatrix,ground_truth_labelVector,all_label_dist,no_of_nodes,no_of_labels,no_of_attrs = ppi_data_utils.load_ppi_data()
        #print "ground_truth_labelVector : ",ground_truth_labelVector
        #print "all_label_dist : ",all_label_dist
        # pre-compute latent graphs for data-set
        if flags[2]==1 :
            # construction of attribute similarity graph
            T1 = np.zeros((no_of_nodes,no_of_nodes))
            for i in range(no_of_nodes) :
                for j in range(no_of_nodes) :
                    if i != j :
                        cos_sim = 1 - spatial.distance.cosine(viewMatrix[i], viewMatrix[j])
                        T1[i][j] = cos_sim
                        T1[j][i] = cos_sim
                    else :
                        T1[i][i] = 1.0
            # construction of even step random walk similarity graph
            T2 = ppi_data_utils.compute_ERWR_Score(no_of_nodes,adjMatrix)
            D = [T1,adjMatrix,T2]
            #D = [adjMatrix,T2]
    elif dataset == 'rugby' :
        data_dict = rugby_data_dict
        N,no_of_nodes,C,no_of_labels,labelMatrix,F,no_of_attrs,viewMatrix,E1,E2,E3,ground_truth_labelVector, all_label_dist = rugby_data_utils.load_Rugby_Data()
        adjMatrix = E1
        if flags[2]==1 :
            #G = [E1,E1.T,E2,E2.T,E3,E3.T] 
            G = [E1]
            # pre-compute latent graphs for data-set
            # construction of attribute similarity graph
            '''T2 = np.zeros((no_of_nodes,no_of_nodes))
            for i in range(no_of_nodes) :
                for j in range(no_of_nodes) :
                    if i != j :
                        cos_sim = cosine_similarity(viewMatrix[i], viewMatrix[j])
                        T2[i][j] = cos_sim
                        T2[j][i] = cos_sim'''
            # construction of even step random walk similarity graph
            T9 = rugby_data_utils.compute_ERWR_Score(no_of_nodes,E1)
            #T10 = rugby_data_utils.compute_ERWR_Score(no_of_nodes,E2)
            #T11 = rugby_data_utils.compute_ERWR_Score(no_of_nodes,E3)
            #T12 = rugby_data_utils.compute_ERWR_Score(no_of_nodes,E1.T)
            #T13 = rugby_data_utils.compute_ERWR_Score(no_of_nodes,E2.T)
            #T14 = rugby_data_utils.compute_ERWR_Score(no_of_nodes,E3.T)
            #D = [T2,E1,E2,E3,E1.T,E2.T,E3.T,T9,T10,T11,T12,T13,T14]
            #D = [E1,E2,E3,E1.T,E2.T,E3.T,T9,T10,T11,T12,T13,T14]
            D = [E1,T9]
    else : 
        print'Wrong dataset name in configuration file.\n'
    
    #Dataset dimension checking code
    #print 'labelMatrix : ',np.shape(labelMatrix)
    #print 'adjMatrix : ',np.shape(adjMatrix)
    #print 'viewMatrix : ',np.shape(viewMatrix)
    #print 'no_of_nodes :',no_of_nodes
    #print 'no_of_labels :',no_of_labels
    #print 'no_of_attrs :',no_of_attrs
    #print 'all_label_dist :',all_label_dist
    
    #Standard run of NMF with the dataset
    '''model = NMF(n_components=2, init='random', random_state=0)
    model.fit(viewMatrix)
    NMF(alpha=0.0, beta=1, eta=0.1, init='random', l1_ratio=0.0, max_iter=200,
      n_components=2, nls_max_iter=2000, random_state=0, shuffle=False,
      solver='cd', sparseness=None, tol=0.0001, verbose=0)
    print "model.components_ : ",model.components_
    print "reconstruction_err_ : ",model.reconstruction_err_'''
    
    choices = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    #choices = [7]
    for selection in choices :
    #while (True) :
        print('|    1: Atribute latent-space classification + Factorize label matrix    |')
        print('|    2: Network latent-space classification + Factorize label matrix    |')
        print('|    3: Improved Network latent-space classification + Factorize label matrix    |')
        print('|    4: Atribute + Network shared representation learning + Factorize label matrix    |')
        print('|    5: Atribute + Improved Network shared representation learning + Factorize label matrix    |')
        print('|    6: Atribute + Network shared representation learning + Network regularization + Factorize label matrix    |')
        print('|    7: Atribute + Network shared representation learning + Network regularization + Latent Graph Integration + Factorize label matrix    |')
        print('|    8: Atribute + Improved Network shared representation learning + Network regularization + Latent Graph Integration + Factorize label matrix    |')
        print('|    9: Atribute latent-space classification + Network regularization  + Factorize label matrix    |')
        print('|    10: Atribute latent-space classification + Network regularization  + Latent Graph Integration + Factorize label matrix    |')
        print('|    11: Network latent-space classification  + Network regularization  + Factorize label matrix    |')
        print('|    12: Network latent-space classification  + Network regularization  + Latent Graph Integration  + Factorize label matrix    |')
        print('|    13: Improved Network latent-space classification  + Network regularization  + Latent Graph Integration + Factorize label matrix    |')
        h_row = 2  
        #selection = int(input("Enter your choice : "))
        if selection in choices :
            name = "SingleRelationalLatentSpaceOnlyClassification_"+str(selection)+".xlsx"
            workbook = xlsxwriter.Workbook(name)
            worksheet = workbook.add_worksheet()
            worksheet.write(0, 1, "Coverage_Error")
            worksheet.write(0, 2, "Exact Match Ratio")
            worksheet.write(0, 3, "Hamming Loss")
            worksheet.write(0, 4, "Accuracy")
            worksheet.set_column(0, 1, 20)
            worksheet.set_column(0, 2, 20)
            worksheet.set_column(0, 3, 20)
            worksheet.set_column(0, 4, 20)
            for k , v in sorted(data_dict.items()) :
                h_col =  0
                avg_accuracy = 0.
                avg_exact_match_ratio = 0.
                avg_coverage_err = 0.
                avg_hamm_loss =0.
                worksheet.write(h_row, h_col, k)
                itr = 0
                for fold in v[0] :
                    print("[ Algorithm running ] info : k - %d , fold - %s\n" % (k,v[0][itr]))
                    x_train=[]
                    xx_train = []
                    xxx_train = []
                    y_train = []
                    x_test=[]
                    xx_test = []
                    xxx_test = []
                    expected = []
                    accuracy = 0.
                    exact_match_ratio = 0.
                    coverage_err = 0.
                    hamm_loss =0.
                    h_col = 1
                    L = np.zeros((no_of_nodes,no_of_nodes))
                        
                    if dataset == 'ppi' :
                        relInfo = ppi_data_utils.generate_rel_info(adjMatrix, labelMatrix)
                        index, train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testViewMatrix,testLabels,testList,testRelInfo = ppi_data_utils.ppi_test_train_split(v[1], fold, viewMatrix, ground_truth_labelVector, relInfo)
                        if flags[2] == 1 :
                            # latent graph integration
                            L = ppi_data_utils.latent_Graph_Generate_Integrate(index,train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testList,viewMatrix,labelMatrix,no_of_labels,D)
                    elif dataset == 'rugby' :
                        relInfo = rugby_data_utils.generate_Rel_Info(E1,E2,E3,labelMatrix)
                        index, train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testViewMatrix,testLabels,testList,testRelInfo = rugby_data_utils.rugby_Test_Train_Split(v[1], fold, viewMatrix, ground_truth_labelVector, relInfo)
                        if flags[2] == 1 :
                            # latent graph integration
                            L = rugby_data_utils.latent_Graph_Generate_Integrate(index,train_sample_count,trainViewMatrix,trainLabels,trainList,trainRelInfo,testList,viewMatrix,labelMatrix,no_of_labels,D)
                    else : 
                        print'Wrong dataset name in configuration file.\n'
                    e_log = "Results/" + "E.log"
                    savetxt(e_log, L)
                    # Creation of penalty matrix from label matrix and training data
                    WW = np.zeros((no_of_nodes,no_of_labels))
                    if flags[3] == 1 :
                        pos=0
                        for i in index:
                            if i==1:
                                j = 0
                                for field in labelMatrix[pos]:
                                    if field == 1 :
                                        WW[pos][j] = 0.01
                                    elif field == 0 :
                                        WW[pos][j] = 1 
                                    j = j + 1
                            pos+=1 
                    WW = WW.T 
                    # factorizing based on input choice
                    A = np.zeros((no_of_nodes,no_of_labels))
                    NMF_MU_obj = NMF_MU(**kargs)
                    A, V, rec = NMF_MU_obj.run(fi, selection, viewMatrix.T, adjMatrix.T, L.T, labelMatrix.T, WW, index, path="Results/")
                    pos=0
                    for i in index:
                        if i==1:
                            x_train.append(A[pos])
                            xx_train.append(viewMatrix[pos])
                            xxx_train.append(adjMatrix[pos])
                            y_train.append(labelMatrix[pos])
                        else:
                            x_test.append(A[pos])
                            xx_test.append(viewMatrix[pos])
                            xxx_test.append(adjMatrix[pos])
                            expected.append(labelMatrix[pos])
                        pos+=1 
                    #print 'x_train ',x_train
                    #print 'y_train ',y_train
                    #print 'x_test ',x_test
                    #print 'expected ',expected
                    predicted = np.array(x_test)
                    binarized_predicted = predicted
                    binarized_predicted[binarized_predicted >= 0.5] = 1
                    binarized_predicted[binarized_predicted < 0.5] = 0
                    coverage_err = coverage_error(np.array(expected), predicted)
                    print 'coverage_err : ',coverage_err
                    exact_match_ratio = accuracy_score(np.array(expected), binarized_predicted, normalize=True, sample_weight=None)
                    hamm_loss = hamming_loss(np.array(expected), binarized_predicted)
                    accuracy = hamming_score(np.array(expected), binarized_predicted)
                    avg_accuracy += accuracy
                    avg_exact_match_ratio += exact_match_ratio
                    avg_coverage_err += coverage_err
                    avg_hamm_loss += hamm_loss
                    worksheet.write(h_row, h_col, coverage_err)
                    worksheet.write(h_row, h_col+1, exact_match_ratio)
                    worksheet.write(h_row, h_col+2, hamm_loss)
                    worksheet.write(h_row, h_col+3, accuracy)
                    h_row += 1
                    itr += 1
                avg_accuracy /= len(v[0])
                avg_exact_match_ratio /= len(v[0])
                avg_coverage_err /= len(v[0])
                avg_hamm_loss /= len(v[0])
                worksheet.write(h_row, h_col-1, "Average : ")
                worksheet.write(h_row, h_col, avg_coverage_err)
                worksheet.write(h_row, h_col+1, avg_exact_match_ratio)
                worksheet.write(h_row, h_col+2, avg_hamm_loss)
                worksheet.write(h_row, h_col+3, avg_accuracy)
                h_row += 2
            workbook.close() 
            
        else : 
            print("Something went wrong!")
            break
        
        
    print("--- %s seconds ---" % (time.time() - start_time))
    fi.close()
    sys.exit(0)  
           
if __name__=='__main__' :
    main_algo_test()
