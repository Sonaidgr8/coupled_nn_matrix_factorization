from numpy import dot, loadtxt, ones
from numpy.random import randint
from numpy.random import random_integers
from scipy.sparse import csr_matrix
import numpy as np
import fnmatch
import os
import scipy.sparse as sp
from scipy import linalg
import numbers
from math import sqrt
import time
from scipy.sparse.linalg import eigsh

def check_non_negative(X, whom):
    X = X.data if sp.issparse(X) else X
    if (X < 0).any():
        raise ValueError("Negative values in data passed to %s" % whom)

def squareFrobeniusNormOfSparseBoolean(M):
    rows, cols = M.nonzero()
    return len(rows) 

def squareFrobeniusNormOfSparse(M):
    rows, cols = M.nonzero()
    norm = 0
    for i in range(len(rows)):
        norm += M[rows[i],cols[i]] ** 2
    return norm

def norm(x):
    x = np.ravel(x)
    return sqrt(np.dot(x, x))

def trace(M):
    return sum(M.diagonal())

def fitNorm(X, A, R):   
    return squareFrobeniusNormOfSparse(X) + fitNormWithoutNormG(X, A, R)

def fitNormWithoutNormG(G, A, R):
    AtA = dot(A.T, A)
    secondTerm = dot(A.T, dot(G.dot(A), R.T))
    thirdTerm = dot(dot(AtA, R), dot(AtA, R.T))
    return np.trace(thirdTerm) - 2 * trace(secondTerm)
    

def reservoir(it, k):
    ls = [next(it) for _ in range(k)]
    for i, x in enumerate(it, k + 1):
        j = randint(0, i)
        if j < k:
            ls[j] = x
    return ls  

def checkingIndices(M, ratio = 1):
    rowSize, colSize = M.shape
    nonzeroRows, nonzeroCols = M.nonzero()
    nonzeroIndices = [(nonzeroRows[i], nonzeroCols[i]) for i in range(len(nonzeroRows))]                
    sampledRows = random_integers(0, rowSize - 1, round(ratio*colSize))
    sampledCols = random_integers(0, colSize - 1, round(ratio*colSize))
    sampledIndices = zip(sampledRows, sampledCols)
    indices = list(set(sampledIndices + nonzeroIndices))
    return indices

def fitNormWithoutNormX(X, A, R):
    AtA = dot(A.T, A)
    secondTerm = dot(A.T, dot(X.dot(A), R.T))
    thirdTerm = dot(dot(AtA, R), dot(AtA, R.T))
    return np.trace(thirdTerm) - 2 * trace(secondTerm)

def matrixFitNormWithoutNormD(D, A, V):
    thirdTerm = dot(dot(V, A.T), dot(A, V.T))
    secondTerm = dot(D.dot(A),V.T)
    return np.trace(thirdTerm) - 2 * trace(secondTerm)

def fitNormWWYAB(WW,Y,A,B):
    firstTerm = dot((WW * Y), Y.T)
    thirdTerm = dot(dot(WW * dot(B, A.T), A), B.T)
    secondTerm = dot(dot((WW * Y), A), B.T)
    return np.trace(firstTerm) + np.trace(thirdTerm) - 2 * trace(secondTerm) 

